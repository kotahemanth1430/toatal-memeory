import java.lang.Runtime;

public class MemoryExample {
    public static void main(String[] args) {
        // Get a reference to the Runtime class
        Runtime runtime = Runtime.getRuntime();

        // Print out the total memory available to the JVM
        long totalMemory = runtime.totalMemory();
        System.out.println("Total Memory: " + totalMemory + " bytes");
    }
}