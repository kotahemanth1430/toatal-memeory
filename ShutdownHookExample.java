import java.lang.Runtime;

public class ShutdownHookExample {
    public static void main(String[] args) {
        // Get a reference to the Runtime class
        Runtime runtime = Runtime.getRuntime();

        // Register a shutdown hook
        Thread shutdownHook = new Thread(() -> {
            System.out.println("Shutdown hook executed");
            // Perform any necessary cleanup or finalization tasks here
        });

        runtime.addShutdownHook(shutdownHook);

        // Perform your program logic here

        // When the program terminates, the shutdown hook will be executed
    }
}
